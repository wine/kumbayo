/*
 * Unit tests for shell32 dialog functions
 *
 * Copyright 2008 Peter Oberndorfer
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

 /* NOTE: file copied from string.c */

#include <stdarg.h>
#include <stdio.h>

#define WINE_NOWINSOCK
#include "windef.h"
#include "winbase.h"
#include "wtypes.h"

#include "wine/test.h"

static HMODULE hShell32;

/* Dialog procedure for RunFileDlg */
static INT_PTR CALLBACK RunDlgProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    trace("proc %d\n", message);
    switch (message)
    {
        case WM_INITDIALOG :
            return TRUE;

        case WM_COMMAND :
            if(LOWORD (wParam) == IDOK || LOWORD (wParam) == IDCANCEL)
                EndDialog (hwnd, 0) ;
            return TRUE ;
    }
    return FALSE;
}


static void test_RunFileDlg(void)
{
    BOOL ret;
    const WCHAR aW[] = {'A', 0};
    const WCHAR cW[] = {'C', ':', '\\', 0};
    HICON icon, icon2;
    INT_PTR result;
    BOOL (WINAPI *pRunFileDlg)(HWND hwndOwner, HICON hIcon, LPCSTR lpstrDirectory, LPCSTR lpstrTitle, LPCSTR lpstrDescription, UINT uFlags);

    pRunFileDlg = (void*) GetProcAddress(hShell32, (LPCSTR)61);
    if(!pRunFileDlg)
    {
        skip("could not find RunFileDlg\n");
        return;
    }

    if(!winetest_interactive)
    {
        skip("not interactive -> not testing RunFileDlg\n");
        return;
    }

    /* FIXME: this test uses mix of A/W type
       it seems this function is different A/W depending on win/shell version
    */

    icon = LoadIcon(NULL, IDI_EXCLAMATION);
    icon2 = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(156));
    trace("before running %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg(NULL, NULL, NULL, NULL, NULL, 0);
    trace("ret:%d, GLE:%d\n",ret, GetLastError());


    trace("before running2 %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg((HWND)0x12354, NULL, NULL, NULL, NULL, 0);
    trace("ret:%d, GLE:%d\n",ret, GetLastError());

    trace("before running3 %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg(NULL, NULL, NULL, NULL, NULL, 999999999);
    trace("ret:%d, GLE:%d\n",ret, GetLastError());


    trace("before running4 %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg(NULL, icon, NULL, NULL, NULL, 0);
    trace("ret:%d, GLE:%d\n",ret, GetLastError());

    trace("before running5 %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg(NULL, icon2, NULL, NULL, NULL, 0);
    trace("ret:%d, GLE:%d\n",ret, GetLastError());

    trace("before running6 %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg(NULL, icon2, NULL, aW, cW, 0);
    trace("ret:%d, GLE:%d\n",ret, GetLastError());

    trace("before running6 %p\n", pRunFileDlg);
    SetLastError(0xdeadbeef);
    ret = pRunFileDlg(NULL, icon2, NULL, aW, cW, 0x8); /*no label*/
    trace("ret:%d, GLE:%d\n",ret, GetLastError());

    trace("before creating our own\n");
    result = DialogBoxParamA(GetModuleHandle(NULL), MAKEINTRESOURCE(123), /*hwnd*/NULL,
                    RunDlgProc, 0);
    trace("after creating our own %lx %d\n", result, GetLastError());

    return;
}

START_TEST(dialogs)
{
    hShell32 = GetModuleHandleA("shell32.dll");

    test_RunFileDlg();
}
