/* Unit test suite for Ntdll file functions
 *
 * Copyright 2007 Jeff Latimer
 * Copyright 2007 Andrey Turkin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * NOTES
 * We use function pointers here as there is no import library for NTDLL on
 * windows.
 */

#include <stdio.h>
#include <stdarg.h>

#include "ntstatus.h"
/* Define WIN32_NO_STATUS so MSVC does not give us duplicate macro
 * definition errors when we get to winnt.h
 */
#define WIN32_NO_STATUS

#include "wine/test.h"
#include "winternl.h"

#ifndef IO_COMPLETION_ALL_ACCESS
#define IO_COMPLETION_ALL_ACCESS 0x001F0003
#endif

static VOID     (WINAPI *pRtlInitUnicodeString)( PUNICODE_STRING, LPCWSTR );
static VOID     (WINAPI *pRtlFreeUnicodeString)(PUNICODE_STRING);
static NTSTATUS (WINAPI *pNtCreateMailslotFile)( PHANDLE, ULONG, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK,
                                       ULONG, ULONG, ULONG, PLARGE_INTEGER );
static NTSTATUS (WINAPI *pNtReadFile)(HANDLE hFile, HANDLE hEvent,
                                      PIO_APC_ROUTINE apc, void* apc_user,
                                      PIO_STATUS_BLOCK io_status, void* buffer, ULONG length,
                                      PLARGE_INTEGER offset, PULONG key);
static NTSTATUS (WINAPI *pNtWriteFile)(HANDLE hFile, HANDLE hEvent,
                                       PIO_APC_ROUTINE apc, void* apc_user,
                                       PIO_STATUS_BLOCK io_status,
                                       const void* buffer, ULONG length,
                                       PLARGE_INTEGER offset, PULONG key);
static NTSTATUS (WINAPI *pNtCreateSection)(HANDLE*, ACCESS_MASK, const OBJECT_ATTRIBUTES*,
                                       const LARGE_INTEGER*, ULONG, ULONG, HANDLE);
static NTSTATUS (WINAPI *pNtMapViewOfSection)(HANDLE, HANDLE, PVOID*, ULONG, SIZE_T,
                                       const LARGE_INTEGER*, SIZE_T*, SECTION_INHERIT, ULONG, ULONG);
static NTSTATUS (WINAPI *pNtUnmapViewOfSection)(HANDLE, PVOID);
static NTSTATUS (WINAPI *pNtClose)( PHANDLE );
static NTSTATUS (WINAPI *pNtAreMappedFilesTheSame)(PVOID, PVOID);
static NTSTATUS (WINAPI *pNtQuerySection)( HANDLE, SECTION_INFORMATION_CLASS, PVOID, ULONG, PULONG);

static NTSTATUS (WINAPI *pNtCreateIoCompletion)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG);
static NTSTATUS (WINAPI *pNtOpenIoCompletion)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
static NTSTATUS (WINAPI *pNtQueryIoCompletion)(HANDLE, IO_COMPLETION_INFORMATION_CLASS, PVOID, ULONG, PULONG);
static NTSTATUS (WINAPI *pNtRemoveIoCompletion)(HANDLE, PULONG_PTR, PULONG_PTR, PIO_STATUS_BLOCK, PLARGE_INTEGER);
static NTSTATUS (WINAPI *pNtSetIoCompletion)(HANDLE, ULONG_PTR, ULONG_PTR, NTSTATUS, ULONG);
static NTSTATUS (WINAPI *pNtSetInformationFile)(HANDLE, PIO_STATUS_BLOCK, PVOID, ULONG, FILE_INFORMATION_CLASS);

static NTSTATUS (WINAPI *pNtQueryVirtualMemory)(HANDLE,LPCVOID,MEMORY_INFORMATION_CLASS,PVOID,SIZE_T,SIZE_T*);

static inline BOOL is_signaled( HANDLE obj )
{
    return WaitForSingleObject( obj, 0 ) == 0;
}

#define PIPENAME "\\\\.\\pipe\\ntdll_tests_file.c"

static BOOL create_pipe( HANDLE *read, HANDLE *write, ULONG flags, ULONG size )
{
    *read = CreateNamedPipe(PIPENAME, PIPE_ACCESS_INBOUND | flags, PIPE_TYPE_BYTE | PIPE_WAIT,
                            1, size, size, NMPWAIT_USE_DEFAULT_WAIT, NULL);
    ok(*read != INVALID_HANDLE_VALUE, "CreateNamedPipe failed\n");

    *write = CreateFileA(PIPENAME, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, 0);
    ok(*write != INVALID_HANDLE_VALUE, "CreateFile failed (%d)\n", GetLastError());

    return TRUE;
}

static HANDLE create_temp_file( ULONG flags )
{
    char buffer[MAX_PATH];
    HANDLE handle;

    GetTempFileNameA( ".", "foo", 0, buffer );
    handle = CreateFileA(buffer, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                         flags | FILE_FLAG_DELETE_ON_CLOSE, 0);
    ok( handle != INVALID_HANDLE_VALUE, "failed to create temp file\n" );
    return (handle == INVALID_HANDLE_VALUE) ? 0 : handle;
}

#define CVALUE_FIRST 0xfffabbcc
#define CKEY_FIRST 0x1030341
#define CKEY_SECOND 0x132E46

ULONG_PTR completionKey;
IO_STATUS_BLOCK ioSb;
ULONG_PTR completionValue;

static long get_pending_msgs(HANDLE h)
{
    NTSTATUS res;
    ULONG a, req;

    res = pNtQueryIoCompletion( h, IoCompletionBasicInformation, (PVOID)&a, sizeof(a), &req );
    ok( res == STATUS_SUCCESS, "NtQueryIoCompletion failed: %x\n", res );
    if (res != STATUS_SUCCESS) return -1;
    ok( req == sizeof(a), "Unexpected response size: %x\n", req );
    return a;
}

static BOOL get_msg(HANDLE h)
{
    LARGE_INTEGER timeout = {{-10000000*3}};
    DWORD res = pNtRemoveIoCompletion( h, &completionKey, &completionValue, &ioSb, &timeout);
    ok( res == STATUS_SUCCESS, "NtRemoveIoCompletion failed: %x\n", res );
    if (res != STATUS_SUCCESS)
    {
        completionKey = completionValue = 0;
        memset(&ioSb, 0, sizeof(ioSb));
        return FALSE;
    }
    return TRUE;
}


static void WINAPI apc( void *arg, IO_STATUS_BLOCK *iosb, ULONG reserved )
{
    int *count = arg;

    trace( "apc called block %p iosb.status %x iosb.info %lu\n",
           iosb, U(*iosb).Status, iosb->Information );
    (*count)++;
    ok( !reserved, "reserved is not 0: %x\n", reserved );
}

static void read_file_test(void)
{
    const char text[] = "foobar";
    HANDLE handle, read, write;
    NTSTATUS status;
    IO_STATUS_BLOCK iosb;
    DWORD written;
    int apc_count = 0;
    char buffer[128];
    LARGE_INTEGER offset;
    HANDLE event = CreateEventA( NULL, TRUE, FALSE, NULL );

    buffer[0] = 1;

    if (!create_pipe( &read, &write, FILE_FLAG_OVERLAPPED, 4096 )) return;

    /* try read with no data */
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    ok( is_signaled( read ), "read handle is not signaled\n" );
    status = pNtReadFile( read, event, apc, &apc_count, &iosb, buffer, 1, NULL, NULL );
    ok( status == STATUS_PENDING, "wrong status %x\n", status );
    ok( !is_signaled( read ), "read handle is signaled\n" );
    ok( !is_signaled( event ), "event is signaled\n" );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( !apc_count, "apc was called\n" );
    WriteFile( write, buffer, 1, &written, NULL );
    /* iosb updated here by async i/o */
    Sleep(1);  /* FIXME: needed for wine to run the i/o apc  */
    ok( U(iosb).Status == 0, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 1, "wrong info %lu\n", iosb.Information );
    ok( !is_signaled( read ), "read handle is signaled\n" );
    ok( is_signaled( event ), "event is not signaled\n" );
    ok( !apc_count, "apc was called\n" );
    apc_count = 0;
    SleepEx( 1, FALSE ); /* non-alertable sleep */
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc not called\n" );

    /* with no event, the pipe handle itself gets signaled */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    ok( !is_signaled( read ), "read handle is not signaled\n" );
    status = pNtReadFile( read, 0, apc, &apc_count, &iosb, buffer, 1, NULL, NULL );
    ok( status == STATUS_PENDING, "wrong status %x\n", status );
    ok( !is_signaled( read ), "read handle is signaled\n" );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( !apc_count, "apc was called\n" );
    WriteFile( write, buffer, 1, &written, NULL );
    /* iosb updated here by async i/o */
    Sleep(1);  /* FIXME: needed for wine to run the i/o apc  */
    ok( U(iosb).Status == 0, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 1, "wrong info %lu\n", iosb.Information );
    ok( is_signaled( read ), "read handle is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    apc_count = 0;
    SleepEx( 1, FALSE ); /* non-alertable sleep */
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc not called\n" );

    /* now read with data ready */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    ResetEvent( event );
    WriteFile( write, buffer, 1, &written, NULL );
    status = pNtReadFile( read, event, apc, &apc_count, &iosb, buffer, 1, NULL, NULL );
    ok( status == STATUS_SUCCESS, "wrong status %x\n", status );
    ok( U(iosb).Status == 0, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 1, "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is not signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, FALSE ); /* non-alertable sleep */
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc not called\n" );

    /* try read with no data */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    ok( is_signaled( event ), "event is not signaled\n" ); /* check that read resets the event */
    status = pNtReadFile( read, event, apc, &apc_count, &iosb, buffer, 2, NULL, NULL );
    ok( status == STATUS_PENDING, "wrong status %x\n", status );
    ok( !is_signaled( event ), "event is signaled\n" );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( !apc_count, "apc was called\n" );
    WriteFile( write, buffer, 1, &written, NULL );
    /* partial read is good enough */
    Sleep(1);  /* FIXME: needed for wine to run the i/o apc  */
    ok( is_signaled( event ), "event is signaled\n" );
    ok( U(iosb).Status == 0, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 1, "wrong info %lu\n", iosb.Information );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc was not called\n" );

    /* read from disconnected pipe */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    CloseHandle( write );
    status = pNtReadFile( read, event, apc, &apc_count, &iosb, buffer, 1, NULL, NULL );
    ok( status == STATUS_PIPE_BROKEN, "wrong status %x\n", status );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( !is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( !apc_count, "apc was called\n" );
    CloseHandle( read );

    /* read from closed handle */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    SetEvent( event );
    status = pNtReadFile( read, event, apc, &apc_count, &iosb, buffer, 1, NULL, NULL );
    ok( status == STATUS_INVALID_HANDLE, "wrong status %x\n", status );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is signaled\n" );  /* not reset on invalid handle */
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( !apc_count, "apc was called\n" );

    /* disconnect while async read is in progress */
    if (!create_pipe( &read, &write, FILE_FLAG_OVERLAPPED, 4096 )) return;
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    status = pNtReadFile( read, event, apc, &apc_count, &iosb, buffer, 2, NULL, NULL );
    ok( status == STATUS_PENDING, "wrong status %x\n", status );
    ok( !is_signaled( event ), "event is signaled\n" );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( !apc_count, "apc was called\n" );
    CloseHandle( write );
    Sleep(1);  /* FIXME: needed for wine to run the i/o apc  */
    ok( U(iosb).Status == STATUS_PIPE_BROKEN, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0, "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc was not called\n" );
    CloseHandle( read );

    /* now try a real file */
    if (!(handle = create_temp_file( FILE_FLAG_OVERLAPPED ))) return;
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    offset.QuadPart = 0;
    ResetEvent( event );
    pNtWriteFile( handle, event, apc, &apc_count, &iosb, text, strlen(text), &offset, NULL );
    ok( status == STATUS_PENDING, "wrong status %x\n", status );
    ok( U(iosb).Status == STATUS_SUCCESS, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == strlen(text), "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc was not called\n" );

    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    offset.QuadPart = 0;
    ResetEvent( event );
    status = pNtReadFile( handle, event, apc, &apc_count, &iosb, buffer, strlen(text) + 10, &offset, NULL );
    ok( status == STATUS_SUCCESS, "wrong status %x\n", status );
    ok( U(iosb).Status == STATUS_SUCCESS, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == strlen(text), "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc was not called\n" );

    /* read beyond eof */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    offset.QuadPart = strlen(text) + 2;
    status = pNtReadFile( handle, event, apc, &apc_count, &iosb, buffer, 2, &offset, NULL );
    ok( status == STATUS_END_OF_FILE, "wrong status %x\n", status );
    ok( U(iosb).Status == 0xdeadbabe, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == 0xdeadbeef, "wrong info %lu\n", iosb.Information );
    ok( !is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( !apc_count, "apc was called\n" );
    CloseHandle( handle );

    /* now a non-overlapped file */
    if (!(handle = create_temp_file(0))) return;
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    offset.QuadPart = 0;
    pNtWriteFile( handle, event, apc, &apc_count, &iosb, text, strlen(text), &offset, NULL );
    ok( status == STATUS_END_OF_FILE, "wrong status %x\n", status );
    ok( U(iosb).Status == STATUS_SUCCESS, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == strlen(text), "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( apc_count == 1, "apc was not called\n" );

    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    offset.QuadPart = 0;
    ResetEvent( event );
    status = pNtReadFile( handle, event, apc, &apc_count, &iosb, buffer, strlen(text) + 10, &offset, NULL );
    ok( status == STATUS_SUCCESS, "wrong status %x\n", status );
    ok( U(iosb).Status == STATUS_SUCCESS, "wrong status %x\n", U(iosb).Status );
    ok( iosb.Information == strlen(text), "wrong info %lu\n", iosb.Information );
    ok( is_signaled( event ), "event is signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    todo_wine ok( !apc_count, "apc was called\n" );

    /* read beyond eof */
    apc_count = 0;
    U(iosb).Status = 0xdeadbabe;
    iosb.Information = 0xdeadbeef;
    offset.QuadPart = strlen(text) + 2;
    ResetEvent( event );
    status = pNtReadFile( handle, event, apc, &apc_count, &iosb, buffer, 2, &offset, NULL );
    ok( status == STATUS_END_OF_FILE, "wrong status %x\n", status );
    todo_wine ok( U(iosb).Status == STATUS_END_OF_FILE, "wrong status %x\n", U(iosb).Status );
    todo_wine ok( iosb.Information == 0, "wrong info %lu\n", iosb.Information );
    todo_wine ok( is_signaled( event ), "event is not signaled\n" );
    ok( !apc_count, "apc was called\n" );
    SleepEx( 1, TRUE ); /* alertable sleep */
    ok( !apc_count, "apc was called\n" );

    CloseHandle( handle );

    CloseHandle( event );
}

static void nt_mailslot_test(void)
{
    HANDLE hslot;
    ACCESS_MASK DesiredAccess;
    OBJECT_ATTRIBUTES attr;

    ULONG CreateOptions;
    ULONG MailslotQuota;
    ULONG MaxMessageSize;
    LARGE_INTEGER TimeOut;
    IO_STATUS_BLOCK IoStatusBlock;
    NTSTATUS rc;
    UNICODE_STRING str;
    WCHAR buffer1[] = { '\\','?','?','\\','M','A','I','L','S','L','O','T','\\',
                        'R',':','\\','F','R','E','D','\0' };

    TimeOut.QuadPart = -1;

    pRtlInitUnicodeString(&str, buffer1);
    InitializeObjectAttributes(&attr, &str, OBJ_CASE_INSENSITIVE, 0, NULL);
    DesiredAccess = CreateOptions = MailslotQuota = MaxMessageSize = 0;

    /*
     * Check for NULL pointer handling
     */
    rc = pNtCreateMailslotFile(NULL, DesiredAccess,
         &attr, &IoStatusBlock, CreateOptions, MailslotQuota, MaxMessageSize,
         &TimeOut);
    ok( rc == STATUS_ACCESS_VIOLATION, "rc = %x not c0000005 STATUS_ACCESS_VIOLATION\n", rc);

    /*
     * Test to see if the Timeout can be NULL
     */
    rc = pNtCreateMailslotFile(&hslot, DesiredAccess,
         &attr, &IoStatusBlock, CreateOptions, MailslotQuota, MaxMessageSize,
         NULL);
    ok( rc == STATUS_SUCCESS, "rc = %x not STATUS_SUCCESS\n", rc);
    ok( hslot != 0, "Handle is invalid\n");

    if  ( rc == STATUS_SUCCESS ) rc = pNtClose(hslot);

    /*
     * Test that the length field is checked properly
     */
    attr.Length = 0;
    rc = pNtCreateMailslotFile(&hslot, DesiredAccess,
         &attr, &IoStatusBlock, CreateOptions, MailslotQuota, MaxMessageSize,
         &TimeOut);
    todo_wine ok( rc == STATUS_INVALID_PARAMETER, "rc = %x not c000000d STATUS_INVALID_PARAMETER\n", rc);

    if  (rc == STATUS_SUCCESS) pNtClose(hslot);

    attr.Length = sizeof(OBJECT_ATTRIBUTES)+1;
    rc = pNtCreateMailslotFile(&hslot, DesiredAccess,
         &attr, &IoStatusBlock, CreateOptions, MailslotQuota, MaxMessageSize,
         &TimeOut);
    todo_wine ok( rc == STATUS_INVALID_PARAMETER, "rc = %x not c000000d STATUS_INVALID_PARAMETER\n", rc);

    if  (rc == STATUS_SUCCESS) pNtClose(hslot);

    /*
     * Test handling of a NULL unicode string in ObjectName
     */
    InitializeObjectAttributes(&attr, &str, OBJ_CASE_INSENSITIVE, 0, NULL);
    attr.ObjectName = NULL;
    rc = pNtCreateMailslotFile(&hslot, DesiredAccess,
         &attr, &IoStatusBlock, CreateOptions, MailslotQuota, MaxMessageSize,
         &TimeOut);
    ok( rc == STATUS_OBJECT_PATH_SYNTAX_BAD, "rc = %x not c000003b STATUS_OBJECT_PATH_SYNTAX_BAD\n", rc);

    if  (rc == STATUS_SUCCESS) pNtClose(hslot);

    /*
     * Test a valid call
     */
    InitializeObjectAttributes(&attr, &str, OBJ_CASE_INSENSITIVE, 0, NULL);
    rc = pNtCreateMailslotFile(&hslot, DesiredAccess,
         &attr, &IoStatusBlock, CreateOptions, MailslotQuota, MaxMessageSize,
         &TimeOut);
    ok( rc == STATUS_SUCCESS, "Create MailslotFile failed rc = %x %u\n", rc, GetLastError());
    ok( hslot != 0, "Handle is invalid\n");

    rc = pNtClose(hslot);
    ok( rc == STATUS_SUCCESS, "NtClose failed\n");

    pRtlFreeUnicodeString(&str);
}

static void test_iocp_setcompletion(HANDLE h)
{
    NTSTATUS res;
    long count;

    res = pNtSetIoCompletion( h, CKEY_FIRST, CVALUE_FIRST, STATUS_INVALID_DEVICE_REQUEST, 3 );
    ok( res == STATUS_SUCCESS, "NtSetIoCompletion failed: %x\n", res );

    count = get_pending_msgs(h);
    ok( count == 1, "Unexpected msg count: %ld\n", count );

    if (get_msg(h))
    {
        ok( completionKey == CKEY_FIRST, "Invalid completion key: %lx\n", completionKey );
        ok( ioSb.Information == 3, "Invalid ioSb.Information: %ld\n", ioSb.Information );
        ok( U(ioSb).Status == STATUS_INVALID_DEVICE_REQUEST, "Invalid ioSb.Status: %x\n", U(ioSb).Status);
        ok( completionValue == CVALUE_FIRST, "Invalid completion value: %lx\n", completionValue );
    }

    count = get_pending_msgs(h);
    ok( !count, "Unexpected msg count: %ld\n", count );
}

static void test_iocp_fileio(HANDLE h)
{
    static const char pipe_name[] = "\\\\.\\pipe\\iocompletiontestnamedpipe";

    IO_STATUS_BLOCK iosb;
    FILE_COMPLETION_INFORMATION fci = {h, CKEY_SECOND};
    HANDLE hPipeSrv, hPipeClt;
    NTSTATUS res;

    hPipeSrv = CreateNamedPipeA( pipe_name, PIPE_ACCESS_INBOUND, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, 4, 1024, 1024, 1000, NULL );
    ok( hPipeSrv != INVALID_HANDLE_VALUE, "Cannot create named pipe\n" );
    if (hPipeSrv != INVALID_HANDLE_VALUE )
    {
        hPipeClt = CreateFileA( pipe_name, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING | FILE_FLAG_OVERLAPPED, NULL );
        ok( hPipeClt != INVALID_HANDLE_VALUE, "Cannot connect to pipe\n" );
        if (hPipeClt != INVALID_HANDLE_VALUE)
        {
            res = pNtSetInformationFile( hPipeSrv, &iosb, &fci, sizeof(fci), FileCompletionInformation );
            ok( res == STATUS_INVALID_PARAMETER, "Unexpected NtSetInformationFile on non-overlapped handle: %x\n", res );
            CloseHandle(hPipeClt);
        }
        CloseHandle( hPipeSrv );
    }

    hPipeSrv = CreateNamedPipeA( pipe_name, PIPE_ACCESS_INBOUND | FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, 4, 1024, 1024, 1000, NULL );
    ok( hPipeSrv != INVALID_HANDLE_VALUE, "Cannot create named pipe\n" );
    if (hPipeSrv == INVALID_HANDLE_VALUE )
        return;

    hPipeClt = CreateFileA( pipe_name, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING | FILE_FLAG_OVERLAPPED, NULL );
    ok( hPipeClt != INVALID_HANDLE_VALUE, "Cannot connect to pipe\n" );
    if (hPipeClt != INVALID_HANDLE_VALUE)
    {
        OVERLAPPED o = {0,};
        BYTE buf[3];
        DWORD read;
        long count;

        NTSTATUS res = pNtSetInformationFile( hPipeSrv, &iosb, &fci, sizeof(fci), FileCompletionInformation );
        ok( res == STATUS_SUCCESS, "NtSetInformationFile failed: %x\n", res );
        ok( U(iosb).Status == STATUS_SUCCESS, "iosb.Status invalid: %x\n", U(iosb).Status );

        count = get_pending_msgs(h);
        ok( !count, "Unexpected msg count: %ld\n", count );
        ReadFile( hPipeSrv, buf, 3, &read, &o);
        count = get_pending_msgs(h);
        ok( !count, "Unexpected msg count: %ld\n", count );
        WriteFile( hPipeClt, buf, 3, &read, NULL );

        if (get_msg(h))
        {
            ok( completionKey == CKEY_SECOND, "Invalid completion key: %lx\n", completionKey );
            ok( ioSb.Information == 3, "Invalid ioSb.Information: %ld\n", ioSb.Information );
            ok( U(ioSb).Status == STATUS_SUCCESS, "Invalid ioSb.Status: %x\n", U(ioSb).Status);
            ok( completionValue == (ULONG_PTR)&o, "Invalid completion value: %lx\n", completionValue );
        }
        count = get_pending_msgs(h);
        ok( !count, "Unexpected msg count: %ld\n", count );

        WriteFile( hPipeClt, buf, 2, &read, NULL );
        count = get_pending_msgs(h);
        ok( !count, "Unexpected msg count: %ld\n", count );
        ReadFile( hPipeSrv, buf, 2, &read, &o);
        count = get_pending_msgs(h);
        ok( count == 1, "Unexpected msg count: %ld\n", count );
        if (get_msg(h))
        {
            ok( completionKey == CKEY_SECOND, "Invalid completion key: %lx\n", completionKey );
            ok( ioSb.Information == 2, "Invalid ioSb.Information: %ld\n", ioSb.Information );
            ok( U(ioSb).Status == STATUS_SUCCESS, "Invalid ioSb.Status: %x\n", U(ioSb).Status);
            ok( completionValue == (ULONG_PTR)&o, "Invalid completion value: %lx\n", completionValue );
        }

        ReadFile( hPipeSrv, buf, sizeof(buf), &read, &o);
        CloseHandle( hPipeSrv );
        count = get_pending_msgs(h);
        ok( count == 1, "Unexpected msg count: %ld\n", count );
        if (get_msg(h))
        {
            ok( completionKey == CKEY_SECOND, "Invalid completion key: %lx\n", completionKey );
            ok( ioSb.Information == 0, "Invalid ioSb.Information: %ld\n", ioSb.Information );
            /* wine sends wrong status here */
            todo_wine ok( U(ioSb).Status == STATUS_PIPE_BROKEN, "Invalid ioSb.Status: %x\n", U(ioSb).Status);
            ok( completionValue == (ULONG_PTR)&o, "Invalid completion value: %lx\n", completionValue );
        }
    }

    CloseHandle( hPipeClt );
}

static void test_iocompletion(void)
{
    HANDLE h = INVALID_HANDLE_VALUE;
    NTSTATUS res;

    res = pNtCreateIoCompletion( &h, IO_COMPLETION_ALL_ACCESS, NULL, 0);

    ok( res == 0, "NtCreateIoCompletion anonymous failed: %x\n", res );
    ok( h && h != INVALID_HANDLE_VALUE, "Invalid handle returned\n" );

    if ( h && h != INVALID_HANDLE_VALUE)
    {
        test_iocp_setcompletion(h);
        test_iocp_fileio(h);
        pNtClose(h);
    }
}

static HANDLE create_temp_file_with_data()
{
    static const char text[] = "foobar";
    DWORD written;
    HANDLE file;
    BOOL ret;

    file = create_temp_file( 0 );
    if(file == 0)
        return INVALID_HANDLE_VALUE;

    trace("file: %p\n", file);

    /* fill with info */
    ret = WriteFile( file, text, sizeof(text), &written, NULL );
    ok( ret, "WriteFile failed\n" );
    ok( written == sizeof(text), "WriteFile did not write all data\n" );
    return file;
}

#include "pshpack1.h"

#define NUM_RELOCATIONS 2 /* one of them is 0 for termination */
#define NUM_SECTIONS 3
#define BASEADDRESS 0x400000
// #define BASEADDRESS 0x10000000

#define TEXT_RVA 0x1000
#define DATA_RVA 0x2000
#define RELOC_RVA 0x3000

struct section_text_t{
    char push;     /* push ebp */
    char mov[2];   /* mov ebp, esp */
    char mov2;     /* mov eax, 10002000h */ /*fix base address!!!!*/
    DWORD address; /* 10002000h -> this one will be relocated */
    char pop;      /* pop ebp */
    char retn[3];  /* retn 0Ch */
};

struct section_data_t{
    DWORD value; /* this one will be returned from DllMain */
};

struct section_reloc_t
{
    DWORD rva;
    DWORD size;
    WORD  relocs[NUM_RELOCATIONS];
};

static HANDLE create_temp_pe_file()
{
    static const struct
    {
        WORD e_magic;      /* 00: MZ Header signature */
        WORD unused[29];
        DWORD e_lfanew;    /* 3c: Offset to extended header */
        char padding[0x70];/* 40: MS-DOS stub program */
    } dos_header =
    {
        IMAGE_DOS_SIGNATURE, { 0 }, sizeof(dos_header), {0}
    };

    static IMAGE_NT_HEADERS nt_header =
    {
        IMAGE_NT_SIGNATURE, /* Signature */
    #ifdef __i386__
        { IMAGE_FILE_MACHINE_I386, /* Machine */
    #else
    # error You must specify the machine type
    #endif
        NUM_SECTIONS, /* NumberOfSections */
        0x47741994,//0, /* TimeDateStamp */
        0, /* PointerToSymbolTable */
        0, /* NumberOfSymbols */
        sizeof(IMAGE_OPTIONAL_HEADER), /* SizeOfOptionalHeader */
        IMAGE_FILE_EXECUTABLE_IMAGE | IMAGE_FILE_LINE_NUMS_STRIPPED | IMAGE_FILE_LOCAL_SYMS_STRIPPED | IMAGE_FILE_32BIT_MACHINE | IMAGE_FILE_DLL /* Characteristics */
        },
        { IMAGE_NT_OPTIONAL_HDR_MAGIC, /* Magic */
        7,//6, /* MajorLinkerVersion */
        0xa,//0, /* MinorLinkerVersion */
        0x200,//0, /* SizeOfCode */
        0x400,//0, /* SizeOfInitializedData */
        0, /* SizeOfUninitializedData */
        0x1000, /* AddressOfEntryPoint */
        0x1000, /* BaseOfCode */
        0x2000, /* BaseOfData */
        BASEADDRESS, /* ImageBase */
        0x1000, /* SectionAlignment */
        0x200, /* FileAlignment */
        4, /* MajorOperatingSystemVersion */
        0, /* MinorOperatingSystemVersion */
        0,//0x0909, /* MajorImageVersion */
        0,//0x0808, /* MinorImageVersion */
        4, /* MajorSubsystemVersion */
        0, /* MinorSubsystemVersion */
        0, /* Win32VersionValue */
        0x4000, /* SizeOfImage */
        0x400, /* SizeOfHeaders */
        0,//0x01020304, /* CheckSum */
        IMAGE_SUBSYSTEM_WINDOWS_GUI, /* Subsystem */
        IMAGE_DLLCHARACTERISTICS_NO_SEH /*0x4576*/, /* DllCharacteristics */
        0x100000/*0x12345600*/, /* SizeOfStackReserve */
        0x1000/*0x12345601*/, /* SizeOfStackCommit */
        0x1000000/*0x12345602*/, /* SizeOfHeapReserve */
        0x10000/*0x12345603*/, /* SizeOfHeapCommit */
        0/*0x12345604*/, /* LoaderFlags */
        0x10, /* NumberOfRvaAndSizes */
        { /* DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES] */
        { 0, 0 }, /* EXPORT table */
        { 0, 0 }, /* IMPORT table */
        { 0, 0 }, /* RESOURCE table */
        { 0, 0 }, /* EXCEPTION table */
        { 0, 0 }, /* CERTIFICATE table */
        { RELOC_RVA, sizeof(struct section_reloc_t) }, /* BASE RELOCATION table */
        { 0, 0 }, /* DEBUG Directory */
        { 0, 0 }, /* arch specific */
        { 0, 0 }, /* global pointer */
        { 0, 0 }, /* TLS */
        { 0, 0 }, /* load configuration */
        { 0, 0 }, /* bound import */
        { 0, 0 }, /* import */
        { 0, 0 }, /* delay import */
        { 0, 0 }, /* CLI */
        { 0, 0 }  /* end */
        }
        }
    };


    static const struct section_text_t text =
    {
        0x55,
        {0x8b, 0xec},
        0xa1,
        BASEADDRESS + DATA_RVA,
        0x5d,
        {0xc2, 0x0c, 0x00}
    };

    static const struct section_data_t data =
    {
        1
    };

    static const struct section_reloc_t relocations =
    {
        TEXT_RVA,
        sizeof(WORD) * NUM_RELOCATIONS + 8,
        {
          IMAGE_REL_BASED_HIGHLOW << 12 | 0x4,
          0
        }
    };

    static const IMAGE_SECTION_HEADER section_header =
    {
        ".text", /* Name */
        { sizeof(struct section_text_t) }, /* Misc VirtualSize */
        TEXT_RVA, /* VirtualAddress */
        0x200, /* SizeOfRawData */
        0x400, /* PointerToRawData */
        0, /* PointerToRelocations */
        0, /* PointerToLinenumbers */
        0, /* NumberOfRelocations */
        0, /* NumberOfLinenumbers */
        IMAGE_SCN_CNT_CODE | IMAGE_SCN_MEM_EXECUTE | IMAGE_SCN_MEM_READ /* Characteristics */
    };

    static const IMAGE_SECTION_HEADER section_header2 =
    {
        ".data", /* Name */
        { sizeof(struct section_data_t) }, /* Misc VirtualSize */
        DATA_RVA, /* VirtualAddress */
        0x200, /* SizeOfRawData */
        0x600, /* PointerToRawData */
        0, /* PointerToRelocations */
        0, /* PointerToLinenumbers */
        0, /* NumberOfRelocations */
        0, /* NumberOfLinenumbers */
        IMAGE_SCN_CNT_INITIALIZED_DATA | IMAGE_SCN_MEM_READ | IMAGE_SCN_MEM_WRITE/* Characteristics */
    };

    static const IMAGE_SECTION_HEADER section_header3 =
    {
        ".reloc", /* Name */
        { sizeof(struct section_reloc_t) }, /* Misc VirtualSize */
        RELOC_RVA, /* VirtualAddress */
        0x200, /* SizeOfRawData */
        0x800, /* PointerToRawData */
        0, /* PointerToRelocations */
        0, /* PointerToLinenumbers */
        0, /* NumberOfRelocations */
        0, /* NumberOfLinenumbers */
        IMAGE_SCN_CNT_INITIALIZED_DATA | IMAGE_SCN_MEM_DISCARDABLE | IMAGE_SCN_MEM_READ/* Characteristics */
    };
#include "poppack.h"
    char buffer2[MAX_PATH] = "c:\\asdf.dll";
    HANDLE handle;
    DWORD dummy;
//     GetTempFileNameA( ".", "foo", 0, buffer2 );

    trace("temp file name: %s\n", buffer2);

//     handle = CreateFileA(buffer2, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
//                          /*FILE_FLAG_DELETE_ON_CLOSE*/0, 0);
    handle = CreateFileA(buffer2, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, 0);
    if(handle == INVALID_HANDLE_VALUE)
    {
        ok( FALSE, "failed to create temp file %d\n", GetLastError() );
        return INVALID_HANDLE_VALUE;
    }

    ok(WriteFile(handle, &dos_header, sizeof(dos_header), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(WriteFile(handle, &nt_header, sizeof(nt_header), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(WriteFile(handle, &section_header, sizeof(section_header), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(WriteFile(handle, &section_header2, sizeof(section_header2), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(WriteFile(handle, &section_header3, sizeof(section_header3), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(SetFilePointer(handle, 0x400, NULL, FILE_BEGIN) != INVALID_SET_FILE_POINTER,
       "SetFilePointer error %d\n", GetLastError());

    ok(WriteFile(handle, &text, sizeof(text), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(SetFilePointer(handle, 0x600, NULL, FILE_BEGIN) != INVALID_SET_FILE_POINTER,
       "SetFilePointer error %d\n", GetLastError());

    ok(WriteFile(handle, &data, sizeof(data), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(SetFilePointer(handle, 0x800, NULL, FILE_BEGIN) != INVALID_SET_FILE_POINTER,
       "SetFilePointer error %d\n", GetLastError());

    ok(WriteFile(handle, &relocations, sizeof(relocations), &dummy, NULL),
       "WriteFile error %d\n", GetLastError());

    ok(SetFilePointer(handle, 0xa00, NULL, FILE_BEGIN) != INVALID_SET_FILE_POINTER,
       "SetFilePointer error %d\n", GetLastError());

    ok(SetEndOfFile(handle), "SetEndOfFile error %d\n", GetLastError());
//     LARGE_INTEGER newpos;
//     newpos.HighPart = 1;  // 4GB
//     newpos.LowPart = 0x1000;
//
//     SetFilePointerEx(handle, newpos, NULL, FILE_BEGIN);/* blow up file to a bit more than 4 GB */
//     SetEndOfFile(handle);

    /* FIXME: how is the file deleted again? */
    /* return path to check with other functions/reopen */
    return handle;
}

static void NtCreateSection_test(void)
{
    HANDLE file;
    HANDLE section;
    OBJECT_ATTRIBUTES attr;
    LARGE_INTEGER size;
    NTSTATUS status;

    file = create_temp_file_with_data();
//     file = create_temp_pe_file();
    if(file == INVALID_HANDLE_VALUE)
    {
        ok( FALSE, "creating a file failed\n" );
        return;
    }

    InitializeObjectAttributes(&attr, NULL, OBJ_CASE_INSENSITIVE, 0, NULL);
    size.QuadPart = 0;
    section = INVALID_HANDLE_VALUE;


/* NTSTATUS WINAPI NtCreateSection( HANDLE *handle, ACCESS_MASK access, const OBJECT_ATTRIBUTES *attr,
                                    const LARGE_INTEGER *size, ULONG protect,
                                    ULONG sec_flags, HANDLE file ) */

    /* null object attributes */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, NULL, &size,
                              PAGE_READWRITE, SEC_COMMIT, file);
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;

    if(0)
    {
        /* NULL out param (crashes wine)*/
        status = pNtCreateSection(NULL, SECTION_ALL_ACCESS, &attr, &size,
                                  PAGE_READWRITE, SEC_COMMIT, file);
        ok( status == STATUS_ACCESS_VIOLATION, "NtCreateSection failed with %x\n", status );
    }

    /* NULL size */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, NULL,
                              PAGE_READWRITE, SEC_COMMIT, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;

//     /* invalid file handle but no SEC_FILE/SEC_COMMIT */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_NOCACHE, (HFILE)0xdeadbeef );
//     todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
//     if(status == STATUS_SUCCESS)
//         /* remove when wine is fixed */
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0;

    /* no commit/reserve flags */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              PAGE_READWRITE, 0, file );
    todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        /* remove when wine is fixed */
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;

//     /* only SEC_NOCHACE flag */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_NOCACHE, file );
//     todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
//     if(status == STATUS_SUCCESS)
//         /* remove when wine is fixed */
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0;
//
//     /* only SEC_LARGE_PAGES flag */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_LARGE_PAGES, file );
//     todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
//     if(status == STATUS_SUCCESS)
//         /* remove when wine is fixed */
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0;

    /* reserve flag with file handle */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              PAGE_READWRITE, SEC_RESERVE , file );
    todo_wine ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;


//     /* reserve flag with file handle */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_RESERVE , (HANDLE) 0xdeadbeef );
//     todo_wine ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );//FIXME: check if file is used or not
//     if(status == STATUS_SUCCESS)
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0;
//
//
//     /* reserve flag with file handle */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_RESERVE|SEC_FILE, (HANDLE) 0xdeadbeef );
//     todo_wine ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );//FIXME: check if file is used or not
//     if(status == STATUS_SUCCESS)
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0x1000;
//
//     /* reserve flag with file handle */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_COMMIT, file );
//     todo_wine ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );//FIXME: check if file is used or not
//     if(status == STATUS_SUCCESS)
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0x1000;
//
//
//     /* reserve flag with file handle */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_COMMIT , (HANDLE) 0xdeadbeef );
//     todo_wine ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );//FIXME: check if file is used or not
//     if(status == STATUS_SUCCESS)
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0x1000;
//
//
//     /* reserve flag with file handle */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_COMMIT|SEC_FILE , (HANDLE) 0xdeadbeef );
//     todo_wine ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );//FIXME: check if file is used or not
//     if(status == STATUS_SUCCESS)
//         pNtClose(section);
//
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0;

    /* invalid protection flags */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              STANDARD_RIGHTS_REQUIRED | SECTION_QUERY | SECTION_MAP_READ, SEC_COMMIT,
                              file );
    todo_wine ok( status == STATUS_INVALID_PAGE_PROTECTION, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        /* remove when wine is fixed */
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;


    /* image and commit at the same time */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              PAGE_READWRITE, SEC_COMMIT | SEC_IMAGE, file );
    todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;


//     /* image and file at the same time */
//     status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
//                               PAGE_READWRITE, SEC_FILE | SEC_IMAGE, file );
//     todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
//     if(status == STATUS_SUCCESS)
//         pNtClose(section);
// 
//     section = INVALID_HANDLE_VALUE;
//     size.QuadPart = 0;


    /* reserve and commit at the same time */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              PAGE_READWRITE, SEC_RESERVE | SEC_IMAGE, file );
    todo_wine ok( status == STATUS_INVALID_PARAMETER_6, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;

    /* image flag on non PE file */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              PAGE_READWRITE, SEC_IMAGE, file );
    todo_wine ok( status == STATUS_INVALID_IMAGE_NOT_MZ, "NtCreateSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
        pNtClose(section);

    section = INVALID_HANDLE_VALUE;
    size.QuadPart = 0;


    /* working */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, &attr, &size,
                              PAGE_READWRITE, SEC_COMMIT, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );

    status = pNtClose(section);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );

    status = pNtClose(file);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );
}

static void NtMapViewOfSection_test(void)
{
    NTSTATUS status;
    PVOID mapping_address, mapping_address2;
    LARGE_INTEGER offset;
    LARGE_INTEGER size;
    HANDLE section = INVALID_HANDLE_VALUE;
    HANDLE file;
    SIZE_T mapping_size;

    file = /*create_temp_file_with_data*/create_temp_pe_file();
    if(file == INVALID_HANDLE_VALUE)
    {
        ok( FALSE, "creating a file failed\n" );
        return;
    }

    size.QuadPart = 0;

    /* working */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, NULL, &size,
                              PAGE_READWRITE, SEC_COMMIT, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );

/* NTSTATUS WINAPI NtMapViewOfSection( HANDLE handle, HANDLE process, PVOID *addr_ptr, ULONG zero_bits,
                                       SIZE_T commit_size, const LARGE_INTEGER *offset_ptr, SIZE_T *size_ptr,
                                       SECTION_INHERIT inherit, ULONG alloc_type, ULONG protect ) */

    mapping_address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* NULL section */
    status = pNtMapViewOfSection(NULL, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_INVALID_HANDLE, "NtMapViewOfSection failed with %x\n", status );
    trace("mapping_size: %lx\n", mapping_size);
    mapping_address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* INVALID_HANDLE section */
    status = pNtMapViewOfSection(INVALID_HANDLE_VALUE, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_OBJECT_TYPE_MISMATCH, "NtMapViewOfSection failed with %x\n", status );

    /* NULL offset */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address, 0, 0, NULL,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_SUCCESS, "NtMapViewOfSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
    {
        status = pNtUnmapViewOfSection(NtCurrentProcess(), mapping_address);
        ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );
    }

    mapping_address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    if(0)
    {
        /* NULL size ptr (crashes wine)*/
        status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                     NULL, ViewShare, 0, PAGE_READWRITE);
        ok( status == STATUS_ACCESS_VIOLATION, "NtMapViewOfSection failed with %x\n", status );

        mapping_address = NULL;
        offset.QuadPart = 0;
        mapping_size = 0;
    }

    /* invalid inherit settings */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                 &mapping_size, /* invalid */0, 0, PAGE_READWRITE);

    todo_wine ok( status == STATUS_INVALID_PARAMETER_8, "NtMapViewOfSection failed with %x\n", status );

    if(status == STATUS_SUCCESS)
    {
        /* remove when wine is fixed */
        status = pNtUnmapViewOfSection(NtCurrentProcess(), mapping_address);
        ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );
    }

    mapping_address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* invalid inherit settings */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                 &mapping_size, /* invalid */3, 0, PAGE_READWRITE);
    todo_wine ok( status == STATUS_INVALID_PARAMETER_8, "NtMapViewOfSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
    {
        /* remove when wine is fixed */
        status = pNtUnmapViewOfSection(NtCurrentProcess(), mapping_address);
        ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );
    }

    mapping_address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* invalid protect flags */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, 0);
    todo_wine ok( status == STATUS_INVALID_PAGE_PROTECTION, "NtMapViewOfSection failed with %x\n", status );
    if(status == STATUS_SUCCESS)
    {
        /* remove when wine is fixed */
        status = pNtUnmapViewOfSection(NtCurrentProcess(), mapping_address);
        ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );
    }

    mapping_address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* working */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_SUCCESS, "NtMapViewOfSection failed with %x\n", status );
    ok( mapping_address != NULL, "Mapping address is 0\n" );

    mapping_address2 = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* second mapping */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &mapping_address2, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_SUCCESS, "NtMapViewOfSection failed with %x\n", status );
    ok( mapping_address2 != NULL, "Mapping address is 0\n" );

    ok( mapping_address != mapping_address2, "Should have mapped at different address\n" );

    status = pNtUnmapViewOfSection(NtCurrentProcess(), mapping_address);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );

    status = pNtUnmapViewOfSection(NtCurrentProcess(), mapping_address2);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );

    status = pNtClose(section);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );

    status = pNtClose(file);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );
}


static void NtAreMappedFilesTheSame_test(void)
{
    NTSTATUS status;
    PVOID address, address2, address3, address4;
    LARGE_INTEGER offset;
    LARGE_INTEGER size;
    HANDLE section = INVALID_HANDLE_VALUE;
    HANDLE section2 = INVALID_HANDLE_VALUE;
    HANDLE section3 = INVALID_HANDLE_VALUE;
    HANDLE file;
    SIZE_T mapping_size;

    file = create_temp_pe_file();
    if(file == INVALID_HANDLE_VALUE)
    {
        ok(FALSE, "creating a file failed\n");
        return;
    }

    if(!pNtAreMappedFilesTheSame)
    {
        skip("NtAreMappedFilesTheSame not found\n");
        return;
    }

    size.QuadPart = 0;

    /* working */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, NULL, &size,
                              PAGE_READWRITE, SEC_IMAGE, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status);
    ok( section != INVALID_HANDLE_VALUE, "Invalid section handle\n");

    size.QuadPart = 0;

    /* working */
    status = pNtCreateSection(&section2, SECTION_ALL_ACCESS, NULL, &size,
                              PAGE_READWRITE, SEC_IMAGE, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status);
    ok( section != INVALID_HANDLE_VALUE, "Invalid section handle\n");

    size.QuadPart = 0;

    /* don't map as image */
    status = pNtCreateSection(&section3, SECTION_ALL_ACCESS, NULL, &size,
                              PAGE_READWRITE, SEC_COMMIT, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status);
    ok( section != INVALID_HANDLE_VALUE, "Invalid section handle\n");

    address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* working */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &address, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_SUCCESS || status == STATUS_IMAGE_NOT_AT_BASE, "NtMapViewOfSection failed with %x\n", status);
    ok( address != NULL, "Mapping address is 0\n");

    address2 = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* second mapping */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &address2, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_IMAGE_NOT_AT_BASE, "NtMapViewOfSection failed with %x\n", status);
    ok( address2 != NULL, "Mapping address is 0\n");

    address3 = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* third mapping */
    status = pNtMapViewOfSection(section2, NtCurrentProcess(), &address3, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_IMAGE_NOT_AT_BASE, "NtMapViewOfSection failed with %x\n", status);
    ok( address3 != NULL, "Mapping address is 0\n");

    address4 = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* fourth mapping (section without SEC_IMAGE)*/
    status = pNtMapViewOfSection(section3, NtCurrentProcess(), &address4, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_SUCCESS, "NtMapViewOfSection failed with %x\n", status);
    ok( address3 != NULL, "Mapping address is 0\n");

    ok( address != address2, "Should have mapped at different address\n");
    ok( address != address3, "Should have mapped at different address\n");

    trace("mapping size: %lx\n", mapping_size);

    trace("address1 %p address2 %p\n", address, address2);
    trace("address3 %p address4 %p\n", address3, address4);
    status = pNtAreMappedFilesTheSame(address, address);
    todo_wine ok(status == STATUS_SUCCESS, "Expected STATUS_SUCCESS, got %x\n", status);

    /* offset into mapping */
    status = pNtAreMappedFilesTheSame((char*)address + 3, (char*)address + 3);
    todo_wine ok(status == STATUS_SUCCESS, "Expected STATUS_SUCCESS, got %x\n", status);

    status = pNtAreMappedFilesTheSame((char*)address + 3, address);
    todo_wine ok(status == STATUS_SUCCESS, "Expected STATUS_SUCCESS, got %x\n", status);

    /* different mappings into same section */
    status = pNtAreMappedFilesTheSame(address, address2);
    todo_wine ok(status == STATUS_SUCCESS, "Expected STATUS_SUCCESS, got %x\n", status);

    /* different section on same file */
    status = pNtAreMappedFilesTheSame(address, address3);
    todo_wine ok(status == STATUS_SUCCESS, "Expected STATUS_SUCCESS, got %x\n", status);

    /* does not work, when not mapped with SEC_IMAGE flag */
    status = pNtAreMappedFilesTheSame(address4, address4);
    ok(status == STATUS_NOT_SAME_DEVICE, "Expected STATUS_NOT_SAME_DEVICE, got %x\n", status);

    /* null params */
    status = pNtAreMappedFilesTheSame(NULL, NULL);
    todo_wine ok(status == STATUS_INVALID_ADDRESS, "Expected STATUS_INVALID_ADDRESS, got %x\n", status);

    status = pNtUnmapViewOfSection(NtCurrentProcess(), address);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status);

    status = pNtUnmapViewOfSection(NtCurrentProcess(), address2);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status);

    status = pNtUnmapViewOfSection(NtCurrentProcess(), address3);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status);

    status = pNtUnmapViewOfSection(NtCurrentProcess(), address4);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status);

    status = pNtClose(section);
    ok( status == STATUS_SUCCESS, "NtClose failed\n");

    status = pNtClose(section2);
    ok( status == STATUS_SUCCESS, "NtClose failed\n");

    status = pNtClose(section3);
    ok( status == STATUS_SUCCESS, "NtClose failed\n");

    status = pNtClose(file);
    ok( status == STATUS_SUCCESS, "NtClose failed\n");
}

typedef struct _SECTION_IMAGE_INFORMATION_priv {
    PVOID EntryPoint;
    ULONG StackZeroBits;
    ULONG StackReserved;
    ULONG StackCommit;
    ULONG ImageSubsystem;
    WORD SubsystemVersionLow;
    WORD SubsystemVersionHigh;
    ULONG Unknown1;
    ULONG ImageCharacteristics; //maybe short is enough?
    WORD ImageMachineType;
    WORD Unknown2;
    ULONG LoaderFlags;
    LARGE_INTEGER FileSize;
} SECTION_IMAGE_INFORMATION_priv , *PSECTION_IMAGE_INFORMATION_priv ;

static void NtQuerySection_test()
{
    NTSTATUS status;
    LARGE_INTEGER section_size;
    HANDLE section = INVALID_HANDLE_VALUE;
    HANDLE section2 = INVALID_HANDLE_VALUE;
    SECTION_BASIC_INFORMATION section_basic;
    SECTION_IMAGE_INFORMATION_priv  section_image;
    HANDLE file;
    ULONG infolen, infolen2;

    file = create_temp_pe_file();
    if(file == INVALID_HANDLE_VALUE)
    {
        ok( FALSE, "creating a file failed\n" );
        return;
    }

    if(!pNtQuerySection)
    {
        skip( "NtQuerySection not found\n" );
        return;
    }

    section_size.QuadPart = 0;

    /* working map as image*/
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, NULL, &section_size,
                              PAGE_READWRITE, SEC_IMAGE, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );
    ok( section != INVALID_HANDLE_VALUE, "Invalid section handle\n" );

    /* working map as file*/
    status = pNtCreateSection(&section2, SECTION_ALL_ACCESS, NULL, &section_size,
                              PAGE_READWRITE, SEC_COMMIT, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );
    ok( section2 != INVALID_HANDLE_VALUE, "Invalid section handle\n" );

    infolen = sizeof(SECTION_BASIC_INFORMATION);
    status = pNtQuerySection(section, SectionBasicInformation, &section_basic, infolen, NULL);
    ok( status == STATUS_SUCCESS, "NtQuerySection failed with %x\n", status );

    trace("basic:\n");
    trace("BaseAddress: %x\n", section_basic.BaseAddress);
    trace("Attributes: %x\n",  section_basic.Attributes);
    trace("Size: %x%08x\n",    section_basic.Size.HighPart, section_basic.Size.LowPart);
    /* TODO: test creating with/without SEC_FILE and still passing a file pointer, then query attributes */
    /* TODO: test create NtCreateSection with SEC_IMAGE and something different than PAGE_READWRITE readonly execute, ...
       probably this flag is ignored */

    todo_wine ok( section_basic.BaseAddress == 0, "wrong BaseAddress %x\n", section_basic.BaseAddress );
    todo_wine ok( section_basic.Attributes == (SEC_FILE | SEC_IMAGE), "wrong Attributes %x\n", section_basic.Attributes );
    ok( section_basic.Size.QuadPart == 0x4000, "wrong Size %x%08x\n", section_basic.Size.HighPart, section_basic.Size.LowPart );/*FIXME: determine automatically */

    infolen = sizeof(SECTION_BASIC_INFORMATION) + 0x50;
    infolen2 = 0xdeadbeef;
    status = pNtQuerySection(section, SectionBasicInformation, &section_basic, infolen, &infolen2);
    ok( status == STATUS_SUCCESS, "NtQuerySection failed with %x\n", status );
    ok( infolen2 == sizeof(SECTION_BASIC_INFORMATION), "returned info length wrong %x\n", status );

    infolen = sizeof(SECTION_BASIC_INFORMATION) - 0x10;
    infolen2 = 0xdeadbeef;
    status = pNtQuerySection(section, SectionBasicInformation, &section_basic, infolen, &infolen2);
    ok( status == STATUS_INFO_LENGTH_MISMATCH, "NtQuerySection failed with %x\n", status );
    todo_wine ok( infolen2 == 0xdeadbeef, "returned info length wrong %x\n", infolen2 );

    infolen = sizeof(SECTION_BASIC_INFORMATION);
    infolen2 = 0xdeadbeef;
    status = pNtQuerySection(section, 5/*invalid class*/, &section_basic, infolen, &infolen2);
    ok( status == STATUS_INVALID_INFO_CLASS, "NtQuerySection failed with %x\n", status );
    ok( infolen2 == 0xdeadbeef, "returned info length wrong %x\n", infolen2 );

    infolen = sizeof(SECTION_IMAGE_INFORMATION_priv );
    infolen2 = 0xdeadbeef;
    status = pNtQuerySection(section, SectionImageInformation, &section_image, infolen, &infolen2);
    ok( status == STATUS_SUCCESS, "NtQuerySection failed with %x\n", status );
    ok( infolen2 == sizeof(SECTION_IMAGE_INFORMATION_priv), "returned info length wrong 0x%x\n", infolen2 );

    trace("image:\n");
    trace("EntryPoint: %p\n",           section_image.EntryPoint);
    trace("StackZeroBits: %x\n",        section_image.StackZeroBits);
    trace("StackReserved: %x\n",        section_image.StackReserved);
    trace("StackCommit: %x\n",          section_image.StackCommit);
    trace("ImageSubsystem: %x\n",       section_image.ImageSubsystem);
    trace("SubsystemVersionLow: %x\n",  section_image.SubsystemVersionLow);
    trace("SubsystemVersionHigh: %x\n", section_image.SubsystemVersionHigh);
    trace("Unknown1: %x\n",             section_image.Unknown1);
    trace("ImageCharacteristics: %x\n", section_image.ImageCharacteristics);
    trace("ImageMachineType: %x\n",     section_image.ImageMachineType);
    trace("Unknown2: %x\n",             section_image.Unknown2);
    trace("LoaderFlags: %x\n",          section_image.LoaderFlags);
    trace("FileSize: %x%08x\n",         section_image.FileSize.HighPart, section_image.FileSize.LowPart);
    /* TODO: fill PE file with certain values and verify the fields above */

    infolen = sizeof(SECTION_IMAGE_INFORMATION_priv );
    infolen2 = 0xdeadbeef;
    status = pNtQuerySection(section2, SectionImageInformation, &section_image, infolen, &infolen2);
    todo_wine ok( status == STATUS_SECTION_NOT_IMAGE, "NtQuerySection failed with %x\n", status );
    todo_wine ok( infolen2 == 0xdeadbeef, "returned info length wrong %x\n", infolen2 );

    trace("image maped a second time:\n");
    trace("EntryPoint: %p\n",           section_image.EntryPoint);
    trace("StackZeroBits: %x\n",        section_image.StackZeroBits);
    trace("StackReserved: %x\n",        section_image.StackReserved);
    trace("StackCommit: %x\n",          section_image.StackCommit);
    trace("ImageSubsystem: %x\n",       section_image.ImageSubsystem);
    trace("SubsystemVersionLow: %x\n",  section_image.SubsystemVersionLow);
    trace("SubsystemVersionHigh: %x\n", section_image.SubsystemVersionHigh);
    trace("Unknown1: %x\n",             section_image.Unknown1);
    trace("ImageCharacteristics: %x\n", section_image.ImageCharacteristics);
    trace("ImageMachineType: %x\n",     section_image.ImageMachineType);
    trace("Unknown2: %x\n",             section_image.Unknown2);
    trace("LoaderFlags: %x\n",          section_image.LoaderFlags);
    trace("FileSize: %x%08x\n",         section_image.FileSize.HighPart, section_image.FileSize.LowPart);
    status = pNtClose(section);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );

    status = pNtClose(section2);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );

    status = pNtClose(file);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );
}

static void dump_mem_basic(MEMORY_BASIC_INFORMATION* info)
{
    trace("BaseAddress: %p\n", info->BaseAddress);
    trace("AllocationBase: %p\n", info->AllocationBase);
    trace("AllocationProtect: %x\n", info->AllocationProtect);
    trace("RegionSize: %lx\n", info->RegionSize);
    trace("State: %x\n", info->State);
    trace("Protect: %x\n", info->Protect);
    trace("Type: %x\n", info->Type);
}

static void NtMapViewOfSection_relocation_test()
{
    NTSTATUS status;
    LARGE_INTEGER section_size;
    PVOID address, address2;
    HANDLE section = INVALID_HANDLE_VALUE;
    HANDLE file;
    SIZE_T mapping_size;
    LARGE_INTEGER offset;
    struct section_text_t *section_text;
    struct section_text_t *section_text2;
    MEMORY_BASIC_INFORMATION mem_basic_info;
//     WCHAR filename3[]= {'c', ':', '\\', 't', 'm', 'p', '\\', 'a', 's', 'd', 'f', '.', 'd', 'l', 'l', 0};

    file = create_temp_pe_file();
    if(file == INVALID_HANDLE_VALUE)
    {
        ok( FALSE, "creating a file failed\n" );
        return;
    }

//     test the file is loadable
//     HMODULE loadlib = LoadLibraryW(filename3);
//     trace("loadLibraryW: %p GLE:%d\n", loadlib, GetLastError());
//     FreeLibrary(loadlib);

    section_size.QuadPart = 0;
    /* working */
    status = pNtCreateSection(&section, SECTION_ALL_ACCESS, NULL, &section_size,
                              PAGE_READWRITE, SEC_IMAGE, file );
    ok( status == STATUS_SUCCESS, "NtCreateSection failed with %x\n", status );
    ok( section != INVALID_HANDLE_VALUE, "Invalid section handle\n" );

    address = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* first mapping */
    /* this might succeed or not depending if preferred load address is already occupied */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &address, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_SUCCESS || status == STATUS_IMAGE_NOT_AT_BASE, "NtMapViewOfSection failed with %x\n", status );
    ok( address != NULL, "Mapping address is 0\n" );
    trace("first mapping at: %p\n", address);

    address2 = NULL;
    offset.QuadPart = 0;
    mapping_size = 0;

    /* map a second time so preferred address is surely occupied */
    /* relocation is not done by NtMapViewOfSection but later */
    status = pNtMapViewOfSection(section, NtCurrentProcess(), &address2, 0, 0, &offset,
                                 &mapping_size, ViewShare, 0, PAGE_READWRITE);
    ok( status == STATUS_IMAGE_NOT_AT_BASE, "NtMapViewOfSection failed with %x\n", status );
    ok( address2 != NULL, "Mapping address is 0\n" );
    trace("second mapping at: %p\n", address2);

    section_text = (struct section_text_t*)((char*) address + TEXT_RVA);
    section_text2 = (struct section_text_t*)((char*) address2 + TEXT_RVA);

    status = pNtQueryVirtualMemory(NtCurrentProcess(), ((char*) address + 5), MemoryBasicInformation, &mem_basic_info, sizeof(mem_basic_info), NULL);
    ok( !status, "NtQueryVirtualMemory on header failed %x\n", status);
    ok( mem_basic_info.BaseAddress == address, "BaseAddress not right %p/%p\n", mem_basic_info.BaseAddress, address );
    ok( mem_basic_info.AllocationBase == address, "AllocationBase not right %p/%p\n", mem_basic_info.AllocationBase, address);
    ok( mem_basic_info.AllocationProtect == PAGE_EXECUTE_WRITECOPY, "AllocationProtect not right %x/PAGE_EXECUTE_WRITECOPY\n", mem_basic_info.AllocationProtect);
    ok( mem_basic_info.RegionSize == 0x1000, "RegionSize not right %lx/%x\n", mem_basic_info.RegionSize, 0x1000);
    ok( mem_basic_info.State == MEM_COMMIT, "State not right %x/MEM_COMMIT\n", mem_basic_info.State);
    ok( mem_basic_info.Protect == PAGE_READONLY, "Protect not right %x/PAGE_READONLY\n", mem_basic_info.Protect);
    ok( mem_basic_info.Type == MEM_IMAGE, "Type not right %x/MEM_IMAGE\n", mem_basic_info.Type);
    trace("\nNtQueryVirtualMemory header: %x\n", status);
    dump_mem_basic(&mem_basic_info);

    status = pNtQueryVirtualMemory(NtCurrentProcess(), ((char*) address + TEXT_RVA + 5), MemoryBasicInformation, &mem_basic_info, sizeof(mem_basic_info), NULL);
    ok( !status, "NtQueryVirtualMemory on text section failed %x\n", status);
    ok( mem_basic_info.BaseAddress == ((char*)address + TEXT_RVA), "BaseAddress not right %p/%p\n", mem_basic_info.BaseAddress, ((char*)address + TEXT_RVA) );
    ok( mem_basic_info.AllocationBase == address, "AllocationBase not right %p/%p\n", mem_basic_info.AllocationBase, address);
    ok( mem_basic_info.AllocationProtect == PAGE_EXECUTE_WRITECOPY, "AllocationProtect not right %x/PAGE_EXECUTE_WRITECOPY\n", mem_basic_info.AllocationProtect);
    ok( mem_basic_info.RegionSize == 0x1000, "RegionSize not right %lx/%x\n", mem_basic_info.RegionSize, 0x1000);
    ok( mem_basic_info.State == MEM_COMMIT, "State not right %x/MEM_COMMIT\n", mem_basic_info.State);
    ok( mem_basic_info.Protect == PAGE_EXECUTE_READ, "Protect not right %x/PAGE_EXECUTE_READ\n", mem_basic_info.Protect);
    ok( mem_basic_info.Type == MEM_IMAGE, "Type not right %x/MEM_IMAGE\n", mem_basic_info.Type);
    trace("\nNtQueryVirtualMemory text: %x\n", status);
    dump_mem_basic(&mem_basic_info);

    status = pNtQueryVirtualMemory(NtCurrentProcess(), ((char*) address + DATA_RVA + 5), MemoryBasicInformation, &mem_basic_info, sizeof(mem_basic_info), NULL);
    ok( !status, "NtQueryVirtualMemory on text data failed %x\n", status);
    ok( mem_basic_info.BaseAddress == ((char*)address + DATA_RVA), "BaseAddress not right %p/%p\n", mem_basic_info.BaseAddress, ((char*)address + DATA_RVA));
    ok( mem_basic_info.AllocationBase == address, "AllocationBase not right %p/%p\n", mem_basic_info.AllocationBase, address);
    ok( mem_basic_info.AllocationProtect == PAGE_EXECUTE_WRITECOPY, "AllocationProtect not right %x/PAGE_EXECUTE_WRITECOPY\n", mem_basic_info.AllocationProtect);
    ok( mem_basic_info.RegionSize == 0x1000, "RegionSize not right %lx/%x\n", mem_basic_info.RegionSize, 0x1000);
    ok( mem_basic_info.State == MEM_COMMIT, "State not right %x/MEM_COMMIT\n", mem_basic_info.State);
    ok( mem_basic_info.Protect == PAGE_WRITECOPY, "Protect not right %x/PAGE_WRITECOPY\n", mem_basic_info.Protect);
    ok( mem_basic_info.Type == MEM_IMAGE, "Type not right %x/MEM_IMAGE\n", mem_basic_info.Type);
    trace("\nNtQueryVirtualMemory data: %x\n", status);
    dump_mem_basic(&mem_basic_info);

    status = pNtQueryVirtualMemory(NtCurrentProcess(), ((char*) address + RELOC_RVA + 5), MemoryBasicInformation, &mem_basic_info, sizeof(mem_basic_info), NULL);
    ok( !status, "NtQueryVirtualMemory on reloc section failed %x\n", status);
    ok( mem_basic_info.BaseAddress == ((char*)address + RELOC_RVA), "BaseAddress not right %p/%p\n", mem_basic_info.BaseAddress, ((char*)address + RELOC_RVA) );
    ok( mem_basic_info.AllocationBase == address, "AllocationBase not right %p/%p\n", mem_basic_info.AllocationBase, address);
    ok( mem_basic_info.AllocationProtect == PAGE_EXECUTE_WRITECOPY, "AllocationProtect not right %x/PAGE_EXECUTE_WRITECOPY\n", mem_basic_info.AllocationProtect);
    ok( mem_basic_info.RegionSize == 0x1000, "RegionSize not right %lx/%x\n", mem_basic_info.RegionSize, 0x1000);
    ok( mem_basic_info.State == MEM_COMMIT, "State not right %x/MEM_COMMIT\n", mem_basic_info.State);
    ok( mem_basic_info.Protect == PAGE_READONLY, "Protect not right %x/PAGE_READONLY\n", mem_basic_info.Protect);
    ok( mem_basic_info.Type == MEM_IMAGE, "Type not right %x/MEM_IMAGE\n", mem_basic_info.Type);
    trace("\nNtQueryVirtualMemory reloc: %x\n", status);
    dump_mem_basic(&mem_basic_info);

    trace("relocated var1: %x var2: %x\n", section_text->address, section_text2->address);
    ok( section_text->address == section_text2->address, "second dlls should not be relocated yet %x/%x\n", section_text->address, section_text2->address );
    ok( section_text2->address == BASEADDRESS + DATA_RVA, "address wrong %x/%x\n", section_text->address, BASEADDRESS + DATA_RVA );

    status = pNtUnmapViewOfSection(NtCurrentProcess(), address);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );

    status = pNtUnmapViewOfSection(NtCurrentProcess(), address2);
    ok( status == STATUS_SUCCESS, "NtUnmapViewOfSection failed with %x\n", status );

    status = pNtClose(section);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );

    status = pNtClose(file);
    ok( status == STATUS_SUCCESS, "NtClose failed\n" );
}


START_TEST(file)
{
    HMODULE hntdll = GetModuleHandleA("ntdll.dll");
    if (!hntdll)
    {
        skip("not running on NT, skipping test\n");
        return;
    }

    pRtlFreeUnicodeString   = (void *)GetProcAddress(hntdll, "RtlFreeUnicodeString");
    pRtlInitUnicodeString   = (void *)GetProcAddress(hntdll, "RtlInitUnicodeString");
    pNtCreateMailslotFile   = (void *)GetProcAddress(hntdll, "NtCreateMailslotFile");
    pNtReadFile             = (void *)GetProcAddress(hntdll, "NtReadFile");
    pNtWriteFile            = (void *)GetProcAddress(hntdll, "NtWriteFile");
    pNtClose                = (void *)GetProcAddress(hntdll, "NtClose");
    pNtCreateIoCompletion   = (void *)GetProcAddress(hntdll, "NtCreateIoCompletion");
    pNtOpenIoCompletion     = (void *)GetProcAddress(hntdll, "NtOpenIoCompletion");
    pNtQueryIoCompletion    = (void *)GetProcAddress(hntdll, "NtQueryIoCompletion");
    pNtRemoveIoCompletion   = (void *)GetProcAddress(hntdll, "NtRemoveIoCompletion");
    pNtSetIoCompletion      = (void *)GetProcAddress(hntdll, "NtSetIoCompletion");
    pNtSetInformationFile   = (void *)GetProcAddress(hntdll, "NtSetInformationFile");

    pNtCreateSection        = (void *)GetProcAddress(hntdll, "NtCreateSection");
    pNtMapViewOfSection     = (void *)GetProcAddress(hntdll, "NtMapViewOfSection");
    pNtUnmapViewOfSection   = (void *)GetProcAddress(hntdll, "NtUnmapViewOfSection");
    pNtAreMappedFilesTheSame= (void *)GetProcAddress(hntdll, "NtAreMappedFilesTheSame");
    pNtQuerySection         = (void *)GetProcAddress(hntdll, "NtQuerySection");

    pNtQueryVirtualMemory   = (void *)GetProcAddress(hntdll, "NtQueryVirtualMemory");

    if(FALSE)
    {
    read_file_test();
    nt_mailslot_test();
    test_iocompletion();
    }

    NtCreateSection_test();
    NtMapViewOfSection_test();
    NtAreMappedFilesTheSame_test();
    NtQuerySection_test();
    NtMapViewOfSection_relocation_test();
}
