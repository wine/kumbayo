This is release 0.9.52 of Wine, a free implementation of Windows on Unix.

What's new in this release:
  - Improved graphics tablet support.
  - Support for RPC context handles.
  - Fixes for some longstanding screen depth issues.
  - Implementation of "My Network Places" shell folder.
  - Lots of bug fixes.

Because of lags created by using mirrors, this message may reach you
before the release is available at the public sites. The sources will
be available from the following locations:

  http://ibiblio.org/pub/linux/system/emulators/wine/wine-0.9.52.tar.bz2
  http://prdownloads.sourceforge.net/wine/wine-0.9.52.tar.bz2

Binary packages for various distributions will be available from:

  http://www.winehq.org/site/download

You will find documentation on

  http://www.winehq.org/site/documentation

You can also get the current source directly from the git or CVS
repositories. Check respectively http://www.winehq.org/site/git or
http://www.winehq.org/site/cvs for details.

If you fix something, please submit a patch; instructions on how to do
this can be found at http://www.winehq.org/site/sending_patches

Wine is available thanks to the work of many people. See the file
AUTHORS in the distribution for the complete list.

----------------------------------------------------------------

Changes since 0.9.51:

Alex Villacís Lasso (2):
      user32: Fix regression in DlgDirList caused by modified LB_DIR return behavior, with tests.
      comdlg32: Fix another regression in 3.1-style file dialog from LB_DIR fix.

Alexander Dorofeyev (7):
      ddraw: Fix incorrect WARN text.
      wined3d: Rewrite IWineD3DSurfaceImpl_BltOverride colorfill codepath.
      wined3d: Use IWineD3DDeviceImpl_ClearSurface in IWineD3DDeviceImpl_Clear.
      ole32: Fix wrong timeout check.
      ws2_32: Fix timeout check.
      wined3d: Add zero/near zero vertex rhw special case.
      wined3d: Also update alpha test when stage texture is NULL.

Alexander Nicolaysen Sørnes (1):
      comdlg32: PageSetupDlgW: Load paper orientation in ChangePrinterW.

Alexandre Julliard (30):
      Revert "user32: Moved some 16-bit functions."
      oleaut32/tests: Avoid relying on system-dependent rounding.
      server: Fix possible NULL dereference.
      ntoskrnl.exe: Added implementation for IoCreateDriver and IoDeleteDriver.
      ntoskrnl.exe: Allow returning data in ioctls along with a positive non-zero status.
      user32: Set the WSF_VISIBLE flag on the main window station.
      advapi32: Reimplement RegisterServiceCtrlHandler on top of RegisterServiceCtrlHandlerEx.
      advapi32: Fix RegisterServiceCtrlHandler spec entry.
      advapi32: Use exponential backoff when waiting for a service to start.
      advapi32: Replace the list of services with an array.
      advapi32: Return from StartServiceCtrlDispatcher when all services are stopped.
      kernel32: Forward interrupts in 32-bit code to winedos too.
      server: Use SIGQUIT instead of SIGTERM to terminate a thread.
      advapi32: Move the EnumDependentServicesA/W stubs in service.c where they belong.
      wineboot: Simplify the unnecessarily complex code structure.
      wineboot: Add support for starting NT-style services.
      wineboot: Rewrite wininit.ini processing to use GetPrivateProfileSectionW. Convert to Unicode.
      wineboot: Convert the rest of the code to Unicode.
      wine.inf: Mark the spool service as disabled, it's just a stub.
      include: Added the mountmgr.h header.
      server: Don't count system processes as users of a desktop.
      server: Don't give out full access to the system process event.
      explorer: It no longer needs to be made a system process.
      advapi32: Start non-interactive services in a separate window station.
      ntdll: Increase the buffer size dynamically for relay debug lists.
      kernel32: Store the initial directory as a full path in the process parameters.
      kernel32: Yet another workaround for broken apps in GlobalMemoryStatus.
      kernel32: Initialize MaximumLength of the user params directory too.
      winedos: Add the possibility of refusing to emulate some interrupts.
      wintab32: Fix debug traces to use wine_dbg_sprintf.

Alistair Leslie-Hughes (1):
      msxml3: Fix memory leaks in tests.

Andrew Riedi (1):
      gdiplus: Add GdipCreateHBITMAPFromBitmap() stub.

Andrew Talbot (10):
      kernel32: Remove unneeded casts.
      kernel32: Remove unneeded casts.
      kernel32: Remove unneeded casts.
      mapi32: Remove unneeded casts.
      kernel32: Revert the removal of a cast from a macro.
      mciavi32: Remove unneeded casts.
      mciseq: Remove unneeded casts.
      msacm32: Remove unneeded casts.
      msvcrt: Remove unneeded cast.
      msi: Remove unneeded cast.

Andrey Turkin (5):
      server: I/O completion ports can only be used with overlapped I/O.
      server: Store I/O completion information in async structure.
      ntdll: Implement BindIoCompletionCallback.
      dbghelp: Adjust minidump streams ordering and sizing.
      dbghelp: Treat const addresses as const unsigned.

Aric Stewart (1):
      fonts: Correct External leading for japanese small font and enable fontmetric test for the font.

David Adam (4):
      d3dx8: Add definition for MatrixStack.
      d3dx8: Rename d3dx_core_private.h to d3dx8_private.h.
      d3dx8: Add basic functions and stubs for MatrixStack.
      d3dx8: Implement D3DXCreateMatrixStack.

Dmitry Timoshkov (11):
      user32: Always clip the button painting to the client rectangle.
      gdi32: Add a GdiConvertToDevmodeW test, make it pass under Wine.
      gdi32: When compiling with PSDK headers request latest DEVMODE definition.
      gdi32: In the calculations use a fixed dmSize, not a passed in (possibly too large) one.
      user32: Make sure to setup clipping before any painting is done.
      user32: Set the edit text in a combobox only if combobox has strings.
      include: Add CONTEXT86_EXTENDED_REGISTERS and CONTEXT86_ALL definitions.
      gdi32: Add a test for minimal acceptable DEVMODEA size, make it pass under Wine.
      user32: Add a test for an invalid DEVMODE passed to ChangeDisplaySettings, make it pass under Wine.
      riched20: Remove a redundant parameter from trace.
      user32: Reimplement IsHungAppWindow.

Eric Pouech (1):
      valgrind: Made Wine compliant will latest Valgrind macros (the one removed from 3.3.0 and deprecated since 3.2.0).

Francois Gouget (13):
      rpcrt4: Add an rpcasync.h header stub and fix the RpcErrorStartEnumeration() prototype.
      user32/tests: Use GetProcAddress() on ChangeDisplaySettingsExA() because it is missing on Windows 95.
      msi: Add a trailing '\n' to Wine traces.
      secur32/tests: Dynamically load various APIs and don't link with crypt32.dll to make the test run on Windows 95.
      shell32/tests: Avoid SHDeleteKeyA() because shlwapi.dll is missing on Windows 95.
      shell32/tests: Copy the PathRemoveBackslashA() and PathAddBackslashW() implementations because shlwapi.dll is missing on Windows 95.
      shell32/tests: Use GetProcAddress() on SHGetPathFromIDListW() because it is missing on Windows 95.
      shell32/tests: Use GetProcAddress() on Shell_NotifyIconW() because it is missing on Windows 95.
      shell32/tests: Use GetProcAddress() on SHFileOperationW() because it is missing on Windows 95.
      oleaut32/tests: Avoid SHDeleteKeyW() because shlwapi.dll is missing on Windows 95.
      dinput: Make _dump_cooperativelevel_DI() and _dump_EnumDevices_dwFlags() more self-contained.
      Assorted spelling fixes.
      configure: Work around an Xcode 3.0 bug when detecting the libGL library.

Gerald Pfeifer (1):
      msi: Fix error handling in encode_streamname().

Huw Davies (6):
      include: Add IMimeAllocator.
      inetcomm: Add IMimeAllocator implementation.
      inetcomm: Fix spelling typo.
      inetcomm: Implement IMimeBody_GetParameters.
      inetcomm: Unquote parameter values.
      inetcomm: Implement IMimeBody_GetOffsets.

Ivan Sinitsin (1):
      winefile: Save font settings in registry.

Jacek Caban (24):
      mshtml: Remove not used argument of nsACString_GetData.
      mshtml: Remove not used argument in nsAString_GetData.
      shdocvw: Ignore Exec(CGID_Explorer, 66) calls in tests.
      urlmon: Use flag to store protocol lock state.
      urlmon: Fixed protocol tests.
      urlmon: Added BindProtocol::GetBindString implementation.
      urlmon: Added IServiceProvider implementation to BindProtocol object.
      urlmon: Added BindProtocol::Switch implementation.
      urlmon: Added BindProtocol::Continue implementation.
      urlmon: Added BindProtocol::[Un]LockRequest.
      urlmon: Added more BindProtocol::ReportProgress implementation.
      urlmon: Added more binding tests.
      urlmon: Fixed BindToObject tests.
      mshtml: Use IBindCtx passed to Load in BindToStorage call.
      mshtml: Added AboutProtocolInfo::QueryInfo implementation.
      mshtml: Added ResProtocolInfo::QueryInfo implementation.
      mshtml: Added QueryInfo tests.
      urlmon: Added CoInternetQueryInfo implementation.
      urlmon: Added CoInternetQueryInfo tests.
      urlmon: RegisterNameSpace clean up.
      urlmon: Added [Un]RegisterMimeFilter implementation.
      urlmon: Added mime filters tests.
      urlmon: Use heap_alloc_zero to allocate BindProtocol.
      urlmon: Rename BindProtocol's IInternetProtocolSink methods.

James Hawkins (23):
      msi: Release the record when loading the hash data.
      msi: Add more tests for MsiOpenPackage.
      msi: Validate the parameters of MsiOpenPackage.
      msi: If the package doesn't exist, return ERROR_FILE_NOT_FOUND.
      msi: Verify that the PID_PAGECOUNT and PID_REVNUMBER summary info properties exist.
      msi: Simplify ExpandAnyPath.
      msi: Add tests for MsiEnumClients.
      msi: Validate the parameters of MsiEnumClients.
      msi: Check the user component key for the clients.
      msi: Also check the local system component key for the clients.
      msi: Return ERROR_UNKNOWN_COMPONENT if no products exist.
      msi: Return ERROR_INVALID_PARAMETER if the product list is empty and index is not zero.
      msi: The line control has a height of exactly 2 device units.
      msi: Set the text color after calling the window proc.
      msi: Test sorting a table using a column that is not selected.
      msi: Sort each table of the join separately.
      msi: Free the ordering information.
      msi: Allow NULL parameters to be passed to the local MsiSetProperty.
      msi: Initialize a default COM apartment for custom actions.
      msi: The BS_GROUPBOX style should only be used if the HasBorder attribute is set.
      msi: Fix deleting temporary rows, with tests.
      msi: Allow the not-equal operator in WHERE query string comparisons.
      msi: Allow whitespace after the property name when setting a property in the dialog.

Jeremy White (11):
      wintab32: Store and use the physical device id to match device to cursor.
      wintab32: Do not offset the physical device id by the cursor number.
      wintab32: Add constants for cursor types and use them.
      wintab32: Order the cursor array by the standard Wacom promulgates.
      wintab32: Add additional device tracing.
      wintab32: Compute our physical device characteristics based on the first tablet device that looks like a stylus.
      wintab32: Correctly handle devices with a device id of 0.
      wintab32: Don't return information for non existent cursors.
      wintab32: Implement the ability to return the number of devices and cursors.
      wintab32: Capture the number of buttons earlier, allowing our button maps to be filled in.
      wintab32: Fix the W->A translation for CSR_NAME and CSR_BTNNAMES.

Johannes Stezenbach (3):
      wininet: Implement support for INTERNET_OPTION_VERSION in InternetQueryOptionW.
      wininet: Fix error return code in FindFirstUrlCacheEntryW() stub.
      ws2_32: Set *res = NULL for error return from WS_getaddrinfo().

Kirill K. Smirnov (7):
      winhelp: Remember last added page.
      winhelp: Avoid any keywords comparision during lookup, store pointer to listbox ITEMDATA instead.
      server: Properly notify renderer while activating another screen buffer.
      ws2_32: Fix flags conversion in getnameinfo() function.
      kernel32: ReadConsoleW should wait for at least one character before returning.
      winhelp: When we reuse window structure, old brush should not be reused.
      kernel32: Invalidate local copy of console input event handle while reallocating console.

Lei Zhang (2):
      quartz: Move aggregation tests into separate file.
      quartz: Make filtergraph aggregatable.

Lionel Debroux (2):
      winedevice: Fix memory leak (found by Smatch).
      winspool: Fix memory leak (found by Smatch).

Maarten Lankhorst (3):
      riched20: Implement ECO/EM SELECTIONBAR.
      riched20: Fix bugs in EM_SETOPTIONS.
      winealsa: Add special case for microphone source in mixer.

Marcus Meissner (2):
      msi: Fixed buffer overflow in number parsing.
      d3d9: Initialize pVertexBuffer.

Michael Jung (1):
      ole32: Avoid leaving a critical section twice.

Michael Stefaniuc (3):
      rpcrt4: Add missing LeaveCriticalSection. Found by Smatch.
      secur32/tests: InitFunctionPtrs() needs to use the global crypt32dll variable.
      gdi32: There is no need to cast NULL to a function pointer.

Mike McCormack (1):
      shell32: Implement "My Network Places" shell folder.

Peter Beutner (1):
      d3d9: Add stub for Direct3DShaderValidatorCreate9().

Peter Oberndorfer (2):
      winex11: Remove unused SWAP_INT macro.
      gdi32: Fix a comment typo.

Reece H. Dunn (2):
      include: Added the new Vista messages.
      user32: Added the new Vista messages to spy.

Rico Schüller (3):
      comdlg32: Fix typo.
      kernel32: Fix typo.
      wined3d: Fix some typos.

Rob Shearman (50):
      widl: Clear padding in the buffer due to alignment.
      widl: Return types shouldn't be freed.
      widl: Only base types or reference pointers to base types don't need to be freed.
      include: Add definitions for RPC_FC_SSTRING and RPC_FC_C_SSTRING.
      rpcrt4: Clear padding inserted into the buffer during marshalling because of alignment.
      rpcrt4: Add a FIXME for RPC_FC_P_ALLOCALLNODES in PointerUnmarshall.
      rpcrt4: Fix NdrConformantStringUnmarshall to use buffer memory if possible.
      rpcrt4: Return an error from rpcrt4_conn_tcp_read if recv returns 0.
      rpcrt4: Make a server association when a bind packet is received in the server.
      rpcrt4: Move association code into a separate file.
      hlink: Fix some memory leaks in the tests.
      include: Add more NDR types and function declarations to rpcndr.h.
      include: Add more types and function declarations to the rpcasync.h header file.
      rpcrt4: Don't copy memory from the buffer in NdrConformantStringUnmarshall if we just pointed the memory pointer into the buffer.
      rpcrt4: Add a stub for I_RpcGetCurrentCallHandle.
      include: Add some context handle defines to rpcdcep.h.
      mscoree: Add a stub for GetVersionFromProcess.
      rpcrt4: Implement I_RpcGetCurrentCallHandle.
      widl: The detection of types is highly dependent on the ordering of the various type detection functions.
      widl: Fix the length used when clearing alignment space in generated files.
      rpcrt4: Fix the ALIGN_POINTER_CLEAR macro.
      rpcrt4: Memory should only be cleared in ComplexUnmarshall, not in ComplexMarshall.
      rpcrt4: Initialise memory passed into RPCs in the server test.
      rpcrt4: Don't use BufferEnd in RpcStream_Write.
      rpcrt4: Fix NdrConformantStringUnmarshall to always increment the buffer during unmarshalling.
      ole32: Fix RPC_GetLocalClassObject to wait for 30 seconds, even if messages arrive while we are waiting.
      rpcrt4: Print an error if stub buffer pointer is passed into safe_copy_from_buffer.
      rpcrt4: Fix a memory leak from the get_filename call in the server tests.
      kernel32: Add a test for calling VirtualAlloc on a view of a SEC_RESERVE file mapping.
      widl: Tweak the rules for when to call pfnFree for arrays.
      rpcrt4: Set pStubMsg->BufferMark in NdrConformantVaryingArrayUnmarshall and NdrVaryingArrayUnmarshall.
      widl: Fix the generation of temporary variables for declared arrays.
      widl: Fix pointer detection in structures and arrays.
      include: Remove semi-colons from the end of cpp_quote statements as MIDL likes them.
      rpcrt4: Move low-level NDR context handle functions to a separate file.
      rpcrt4: Implement higher-level NDR server context handle functions.
      rpcrt4: Implement low-level context handle support.
      rpcrt4: Add tests for low-level context handle functions.
      widl: Add support for generating code for out-only context handles by calling NdrContextHandleInitialize.
      rpcss: Use context handles to automatically free running object table entries if the client process quits without calling IrotRevoke.
      hlink: Fix memory leak by freeing extension services This->headers in the release function.
      msi: Fix several memory leaks after using IEnumSTATSTG_Next.
      oleaut32: Fix copy and paste error in VARIANT_UserUnmarshall with VT_UNKNOWN test.
      quartz: Fix memory leak in FilterGraphInner_Release.
      rpcrt4: Add tests for varying and conformant varying arrays.
      rpcrt4: Re-use existing memory for embedded pointers in NdrConformantVaryingArrayUnmarshall.
      rpcrt4: Re-use existing memory for embedded pointers in NdrVaryingArrayUnmarshall.
      rpcss: Zero the memory of several variables before writing them to the pipe to silence Valgrind warnings.
      explorer: Fix memory leak when one or more properties are not present on a hal device.
      server: Initialise the apc_call_t union in async_set_result to all zero to avoid a Valgrind warning.

Roderick Colenbrander (2):
      wined3d: In case of GL_EXT_paletted_textures we use GL_COLOR_INDEX and don't need a GL_RED fixup.
      wined3d: Only store the palette index in the alpha component when the primary render target uses P8.

Roy Shea (2):
      qmgr: Generate C file with local GUID definitions from bits.idl.
      qmgr: AddRef, QueryInterface, and stub functions for queue manager interface.

Stefan Dösinger (37):
      winex11: Distinguish between bpp and depth.
      winex11: Activate the depth-bpp difference.
      d3d9: The refrast returns an error on invalid clear rects.
      d3d9: D3DLOCK_DISCARD is not valid on managed textures.
      d3d9: Do not set NONE MIN and MAG filters.
      d3d9: Remove a few more incorrect D3DLOCK_DISCARDs.
      d3d9: Add a note about a breakage in the refrast.
      wined3d: Disable the SWAPEFFECT_DISCARD clear.
      wined3d: Correct a pixel shader version comparison.
      wined3d: Ignore tesselated and unused streams when comparing attribs.
      wined3d: Filter out some shader compilation spam.
      kernel32: Add a test for BindIoCompletionCallback.
      wined3d: Implement D3DFMT_G16R16.
      wined3d: Split up the render target -> render target blit.
      wined3d: Implement blits from the active render target to swapchains.
      wined3d: Emulate half float vertices if GL_NV_half_float is not there.
      wined3d: Some improvements for SFLAG_CONVERTED checking.
      wined3d: Implement texture -> swapchain blits with stretch_rect_fbo.
      wined3d: Always dump the fbo state on errors.
      wined3d: Store if half float conversion is needed in the decl.
      wined3d: Fixed function vertex attribute types are flexible.
      wined3d: Fix vertex buffers based on type, not semantic.
      wined3d: Move the GL info structure into the adapter.
      wined3d: Add a test for 16 bit floats.
      wined3d: Implement half float vertex buffer conversion.
      wined3d: Change the vertex buffer declaration's data format.
      d3d9: Extend the fixed function vertex declaration test.
      wined3d: Improve some traces.
      wined3d: Add a test for POSITIONT vertex fixups.
      wined3d: Vertex attributes are 4 byte aligned.
      wined3d: Per stream offsets are 4 byte aligned.
      wined3d: Implement and test vertex fixups with per stream offsets.
      d3d9: Accept D3DDEVTYPE_REF devices in the vertex declaration test.
      wined3d: Fix a problem in BltOverride.
      wined3d: Get rid of the invymat.
      wined3d: Combine two glScalef calls into one.
      wined3d: Multiply the half pixel correction with .w.

Stefan Leichter (2):
      gdiplus: Partly implement GdipCreateFromHDC2
      kernel32: Silence a fixme in GetNativeSystemInfo.

Vincent Hardy (1):
      regedit: Window caption correction.

Zhangrong Huang (2):
      gdi32, winex11.drv: Correct default Chinese font name.
      secur32: Fix possible crash while loading SSP providers from registry.

--
Alexandre Julliard
julliard@winehq.org
